package com.socket.client.runClient;

import com.socket.client.socket.SocketClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Created by Евгений on 16.09.2017.
 */
public class MainClient {

    public static void main(String[] args) throws IOException, InterruptedException {

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("example.xml");
        SocketClient socketClient = (SocketClient) applicationContext.getBean("socketClient");
        socketClient.connect();
    }
}
