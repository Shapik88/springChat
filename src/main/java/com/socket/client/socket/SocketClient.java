package com.socket.client.socket;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import static com.socket.utils.SockeUtils.sendMessage;

/**
 * Created by Евгений on 16.09.2017.
 */
@Service
public class SocketClient {
    public final int PORT = 8080;
    public final String IpAdress = "192.168.0.109";
    Scanner scanner = new Scanner(System.in);

    public void connect() {
        try (Socket socket = new Socket(IpAdress, PORT)) {

            System.out.print("Enter your nickname: ");
            String clientName = scanner.nextLine();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
            while (true) {
                String message = scanner.nextLine();
                sendMessage("Client " + clientName + ": " + message, printWriter);

                new Thread(() -> {
                    try {
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            System.out.println("Message received: " + line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();

            }


        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

