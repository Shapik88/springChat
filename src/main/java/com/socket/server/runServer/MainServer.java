package com.socket.server.runServer;

import com.socket.server.socket.SocketServer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Created by Евгений on 16.09.2017.
 */
public class MainServer {
    public static void main(String[] args) throws IOException {

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("example.xml");
        SocketServer socketServer = (SocketServer) applicationContext.getBean("socketServer");
        socketServer.start();

    }
}

