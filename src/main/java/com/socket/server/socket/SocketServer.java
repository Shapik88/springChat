package com.socket.server.socket;

import com.socket.client.socket.SocketClient;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import static com.socket.utils.SockeUtils.sendMessage;


/**
 * Created by Евгений on 16.09.2017.
 */
@Service
public class SocketServer {
    private static final String EXIT = "exit";
    SocketClient socketClient = new SocketClient();
    String line;
    ArrayList<String> chat = new ArrayList<>();
    ArrayList<PrintWriter> clientName = new ArrayList<>();

    public void start() {
        try (ServerSocket serverSocket = new ServerSocket(socketClient.PORT)) {

            while (true) {
                System.out.println("Server waiting!");
                Socket socket = serverSocket.accept();
                new Thread(() -> {
                    try {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
                        if (!clientName.equals(printWriter)) {
                            clientName.add(printWriter);
                        }

                        for (int i = 0; i < chat.size(); i++) {
                            sendMessage(chat.get(i), printWriter);
                        }

                        while ((line = bufferedReader.readLine()) != null) {
                            chat.add(line);
                            new Thread(() -> {
                                for (int i = 0; i < clientName.size(); i++) {
                                    sendMessage(line, clientName.get(i));

                                }
                            }).start();

                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}


