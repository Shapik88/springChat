package com.socket.aplication;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import com.socket.utils.SockeUtils;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Евгений on 17.09.2017.
 */
public class Controller {
    public String NickNameClient;
    public PrintWriter printWriter;
    public Socket socket;
    public BufferedReader bufferedReader;
    SockeUtils sockeUtils = new SockeUtils();
    @FXML
    TextArea AreaMessage;
    @FXML
    TextField NickName;
    @FXML
    TextField Message;
    @FXML
    Label LabelText;
    @FXML
    Button ButtonsendMessage;
    @FXML
    Button ButtonNickName;

    @FXML
    private void NickName(ActionEvent event) {


    }

    @FXML
    private void sendMessage(ActionEvent event) {
        AreaMessage.setText(Message.getText());
        // sockeUtils.sendMessage(NickNameClient + ":  " + Message.getText(), printWriter);
        Message.clear();
    }

}
